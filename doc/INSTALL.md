Basic Installation
==================

```
qmake
make
sudo make install
```
File Location
=============

service file = /lib/systemd/system/

bin file = /usr/local/sbin/

setup file = /etc/tiat/

backend file = /tmp/

Required Dependencies(Ubuntu)
=============================
   * Qt5.9(qt5-default)
   * g++
   * libqt5sql5-psql

   Required Dependencies(CentOS)
================================
   * gcc-c++
   * Qt5.9(qt5-qtbase-devel)
   * qt5-qtbase-postgresql

PostgreSQL Server Installation Example On CentOS 7.5
====================================================

The following is just a basic installation of PostgreSQL and is not a reference to PostgreSQL administration or database security. 
Please refer to the PostgreSQL user manual for a more secure setup.
-------------------------------------------------------------------

```
sudo yum install postgresql-server
sudo su -l postgres
initdb -D /var/lib/pgsql/data/
logout
sudo systemctl enable postgresql
sudo systemctl start postgresql
psql postgres postgres
create database <databasename>;
\quit
psql -U postgres <database> < /tmp/tiat_backend.sql
```

If you have PostgreSQL and Tiat on the same server I recommend configure Tiat to start after PostgreSQL.

You can do that by edit the tiat.service file under /lib/systemd/system/

```
[Unit]
Description=Tiat
After=network.target postgresql.service

[Service]
Type=simple
User=root
ExecStart=/usr/local/sbin/tiat

[Install]
WantedBy=multi-user.target
```
