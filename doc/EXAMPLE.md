Backend Configuration Examples
==============================

Networks
--------

First you need to add relay-agents related to your services.

Let's say my internet service is net 100.64.0.0/24 and I use 100.64.0.1 as relay-agent...

```
INSERT INTO dhcp4_networks (network, relayagent) VALUES ('Internet', '100.64.0.1');
```
And my VoIP service...

```
INSERT INTO dhcp4_networks (network, relayagent) VALUES ('VoIP', '172.16.0.0.1');
```

Subnets
-------

You need to add a subnet for your service.

Here you can choose if the subnet is going to allow unknown clients or not, or if the subnet will be used for IP reservations.

Let's say I want the subnet 100.64.0.0/24 to only hand out IPs to clients that I allow...

```
INSERT INTO dhcp4_subnets (subnet, allowunknown, isstatic, network) VALUES ('100.64.0.0/24', false, false, 1);
```

Allowunknown and isstatic is false by default and "1" is reference to the internet service.

You can have multiple subnets related to a service.

You can for example have one subnet that doesn't allow unknown clients and one that does, or more of the same type if you run out of IPs for a scope.

Pools
-----

To specify the range of available IPs for a subnet...

```
INSERT INTO dhcp4_pools (poolstart, poolend, subnet) VALUES ('100.64.0.10', '100.64.0.254', 1);
```

Where "1" is reference to the 100.64.0.0/24 subnet. 

Pool options
------------

Here you provide options that you want available for a subnet.

The subnet 100.64.0.0/24 will have the options 1, 3, 6 and 51.

1 = subnetmask. 

3 = gateway. 

6 = name server. 

51 = lease time.

You can find more if you search for dhcp options online.

```
INSERT INTO dhcp4_pooloptions (option, value, subnet) VALUES (1, '255.255.255.0', 1);
INSERT INTO dhcp4_pooloptions (option, value, subnet) VALUES (3, '100.64.0.1', 1);
INSERT INTO dhcp4_pooloptions (option, value, subnet) VALUES (6, '8.8.8.8', 1);
INSERT INTO dhcp4_pooloptions (option, value, subnet) VALUES (51, '1200', 1);
```

Where "1" is reference to the 100.64.0.0/24 subnet.

If you want an option to have multiple values you can separate them with a comma. If you have 2 name servers for example ('8.8.8.8,8.8.4.4').

Vendor specific option (Option 43)
----------------------

If you want to provide vendor specific options you can define the option in dhcp4_vendoroptions.

```
INSERT INTO dhcp4_vendoroptions (name, suboption, value, type) values ('HES-3109.configuration-file', 7, 'Test.conf', 1);
```

You can find the available types in the table vendortypes. 1 = String.

You can then add the option to dhcp4_pooloptions or dhcp4_clientoptions with the vendor option name as value.

```
INSERT INTO dhcp4_pooloptions (option, value, subnet) VALUES (43, 'HES-3109.configuration-file', 1);
```

Known clients
-------------

If you've defined a subnet that doesn't allow unknown clients we need to add them to dhcp4_knownclients.

```
INSERT INTO dhcp4_knownclients (clientid, value, ipcount, network) VALUES ('prinfo', 'port1_switch1', 1, 1);
```

Where "1" is reference to the internet service.

Value should be either the client's option82 value or mac-address. Clientid is optional and ipcount is 1 by default.

If you're adding both circuitid and remoteid you need to separate them with an underscore. You can add circuitid alone but not remoteid.

IP reservation
--------------

If you've defined a subnet as "isstatic" you need to add them to dhcp4_staticips.

```
INSERT INTO dhcp4_staticips (ipaddress, client) VALUES ('x.x.x.x', 1);
```

Where 1 is reference to an id from dhcp4_knownclients.

Client specific options
-----------------------

You can add or override an option for a specific client.

Let's say you want one CPE to have a different bootfile than the others.

```
INSERT INTO dhcp4_clientoptions (option, value, client) values (67, 'cpe1.bin', 1);
```

Where 1 is reference to an id from dhcp4_knownclients.

Setup Configuration Example
===========================

	"dhcp": {
		"address": "10.255.255.10"
	},

	"backend": {
		"address": "database.tiat.net",
		"database": "tiatdb",
		"username": "tiatuser",
		"password": "password"
	},

	"hapair": {
		"role": "",
		"address": "",
		"port": "6733"
	}


Master And Slave Setup
======================

You can configure Tiat as master and slave. That way the slave will take over if the master goes down or if the connection to it is lost.

You choose which role the server should have in the setup.conf file.

	"hapair": {
		"role": "master",
		"address": "10.255.255.10",
		"port": "6733"
	}

The address field should be the IP address the master will listen for tcp keepalives, or the IP the slave will send keepalives to.

The default port is 6733 but you can choose whatever port you like.

Test Startup Configuration
==========================

When you have made changes to the configuration you can test it before restarting with the "-t" argument.

```
tiat -t
```

The test will not try to bind the server IP. If you have entered an invalid server IP the test will pass but Tiat will not start.

