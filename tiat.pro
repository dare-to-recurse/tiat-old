QT -= gui
QT += sql
QT += network

TEMPLATE = app
TARGET = tiat
CONFIG += c++11 console

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    src/dhcpdatabase.cpp \
    src/dhcpnetwork.cpp \
    src/dhcpoption.cpp \
    src/dhcpparser.cpp \
    src/dhcpserver.cpp \
    src/dhcpsubnet.cpp \
    src/main.cpp \
    src/tiatha.cpp

HEADERS += \
    src/dhcpdatabase.h \
    src/dhcpnetwork.h \
    src/dhcpoption.h \
    src/dhcpparser.h \
    src/dhcpserver.h \
    src/dhcpsubnet.h \
    src/tiatha.h

DISTFILES += \
    distfiles/setup.conf \
    distfiles/tiat.service \
    distfiles/tiat_backend.sql

target.path += /usr/local/sbin
INSTALLS += target

conf.path += /etc/tiat
conf.files += distfiles/setup.conf
INSTALLS += conf

backend.path += /tmp
backend.files += distfiles/tiat_backend.sql
INSTALLS += backend

systemd.path += /lib/systemd/system
systemd.files += distfiles/tiat.service
INSTALLS += systemd
