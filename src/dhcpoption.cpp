/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#include "dhcpoption.h"
#include <QVector>

DhcpOption::DhcpOption(QObject *parent)
    : QObject(parent)
{
}

//Called with values from database to turn it into QByteArray.
void DhcpOption::append(const char &dhcpOption, const QString &optionValue)
{
    options.append(1, dhcpOption);
    QStringList splitString;

    switch (dhcpOption) {
    case 1:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 28:
    case 42:
    case 69:
    case 81:
        //If value contains "," split the value and multiply the size.
        if(optionValue.contains(',')) {
            splitString = optionValue.split(',');
            options.append(1, 4*splitString.count());
            for(int i = 0; i < splitString.count(); ++i) {
                options.append(ipStringToByteArray(splitString.value(i)));
            }
            break;
        }
        options.append(1, 4);
        options.append(ipStringToByteArray(optionValue));
        break;
    case 2:
    case 51:
        options.append(1, 4);
        options.append(int32StringToByteArray(optionValue));
        break;
    case 15:
    case 66:
    case 67:
        options.append(1, optionValue.size());
        options.append(optionValue);
        break;
    default:
        break;
    }

}

void DhcpOption::append(const QByteArray &dhcpOptions)
{
    options.append(dhcpOptions);
}

QByteArray DhcpOption::ipStringToByteArray(const QString &string)
{
    QStringList split = string.split('.');
    QByteArray ba;
    ba.append(split.value(0).toUShort());
    ba.append(split.value(1).toUShort());
    ba.append(split.value(2).toUShort());
    ba.append(split.value(3).toUShort());

    return ba;
}

QByteArray DhcpOption::int16StringToByteArray(const QString &string)
{
    quint16 i = string.toUInt();
    QVector<uchar> chars;
    chars.append((i >> 8) & 0xFF);
    chars.append(i & 0xFF);

    QByteArray convertedString;
    convertedString.append(chars.value(0));
    convertedString.append(chars.value(1));

    return convertedString;
}

QByteArray DhcpOption::int32StringToByteArray(const QString &string)
{
    quint32 i = string.toUInt();
    QVector<uchar> chars;
    chars.append((i >> 24) & 0xFF);
    chars.append((i >> 16) & 0xFF);
    chars.append((i >> 8) & 0xFF);
    chars.append(i & 0xFF);

    QByteArray convertedString;
    convertedString.append(chars.value(0));
    convertedString.append(chars.value(1));
    convertedString.append(chars.value(2));
    convertedString.append(chars.value(3));

    return convertedString;
}
