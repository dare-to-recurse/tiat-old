/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#ifndef DHCPDATABASE_H
#define DHCPDATABASE_H

#include "dhcpoption.h"
#include "dhcpnetwork.h"
#include <QObject>
#include <QSqlDatabase>
#include <QHostAddress>

class DhcpDatabase : public QObject
{
    Q_OBJECT

public:
    explicit DhcpDatabase(QObject *parent = nullptr);
    bool connectToDatabase(const QMap<QString, QString> &connectionInfo);
    void closeDatabase();
    bool updateLease(const QByteArray &macAddress, const QHostAddress &ipAddress);
    void getExistingLeases();
    void removeLeasesOutOfScope();
    void removeLease(const QHostAddress &leasedIp, const QByteArray &macAddress);
    bool addLease(const QHostAddress &ipAddress, const QByteArray &macAddress,
                  const QString &option82, const quint32 &subnetId);
    bool addLease(const QHostAddress &ipAddress, const QByteArray &macAddress,
                  const quint32 &subnetId);
    bool isFreeLease(const QHostAddress &ipAddress);
    quint32 knownClientsQuery(const QByteArray &macAddress, const QString &option82, const quint32 networkId);
    QHostAddress haveLease(const QByteArray &macAddress, const quint32 &subnetId);
    QHostAddress haveLease(const QByteArray &macAddress);
    QHostAddress staticIpQuery(const quint32 &clientId);
    QByteArray getOptions(const quint32 &subnetId);
    QByteArray getClientOptions(const quint32 &clientId);
    QByteArray getVendorOptions(const quint32 &subnetId);
    QByteArray getClientVendorOptions(const quint32 &clientId);
    QList<DhcpSubnet*> subnetQuery(const quint32 &networkId);
    QList<QHostAddress>* poolQuery(const QString &subnet);
    QList<DhcpNetwork*> networkQuery();
    quint32 clientIpCount(const QString &option82);
    quint32 clientLeases(const QString &option82, const quint32 &networkId);
    quint32 networkIdFromCiaddr(const QHostAddress &ciAddr);

    enum Type {
        String = 1,
        Uint8,
        Uint16,
        Uint32,
        IPAddress,
        Boolean,
        Hex
    };

signals:
    void leaseRemoved(const quint32 &subnetId, const QHostAddress &leasedIp);
    void leasedIp(const quint32 &subnetId, const QHostAddress &leasedIp);

public slots:
    void clearOldLeases();

};

#endif // DHCPDATABASE_H
