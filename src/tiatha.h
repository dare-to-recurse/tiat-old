/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#ifndef TIATHA_H
#define TIATHA_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>

class TiatHA : public QObject
{
    Q_OBJECT
    QTcpServer master;
    QTcpSocket slave;
    QTimer reconnectTimer;
    QMap<QString, QString> connectionInfo;
    QHostAddress address;
    quint16 port;

public:
    explicit TiatHA(const QMap<QString, QString> &connectionInfo, QObject *parent = nullptr);
    void startHaMaster();
    void startHaSlave();

public slots:
    void masterConnectionClosed();
    void checkMasterStatus();
    void reconnectToMaster();

signals:
    void startDhcp(const QMap<QString, QString>& connectionInfo);
    void stopDhcp();
};

#endif // TIATHA_H
