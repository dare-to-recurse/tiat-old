Tiat is a DHCPv4 server that's written with Qt and C++.

It uses PostgreSQL as backend for network configuration and to manage clients.

Tested on Ubuntu 18.04 and CentOS 7.5 and with PostgreSQL 9.5.

Notes
------------------

When the program starts it will query the database to retrieve configured networks, subnets, pools and options and load it in memory.
It also check for existing leases to add or remove from the pools.

When the program receives a packet it will try match the giaddr(Relay Agent IP) with the configured networks.
If there's a match it will query the database for known clients and existing leases and update the database accordingly.
You can configure a subnet to allow unknown clients.

Every 5 seconds the program will check the database connection and try to reconnect if needed, it also query the database to look for leases and remove those who are older than twice its subnet lease time.
If there's no lease time configured for a subnet the lease will never be removed.

You can add and remove clients, client options and IP reservations from the database while the program is running, but if you edit networks, subnets, pools or options the program needs to reload.
Also you should not add or remove a lease manually without restarting the program, the program won't know if the IP is available or not.

Tiat has support for master and slave HA.
The slave will send TCP keepalives to the master every second, and if 10 consecutive probes fail the slave will take over.
When the master comes back up the slave will go back to standby.

For more information see INSTALL.md and EXAMPLE.md.

