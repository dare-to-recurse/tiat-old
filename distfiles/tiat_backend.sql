--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.16
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dhcp4_clientoptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_clientoptions (
    id integer NOT NULL,
    option smallint NOT NULL,
    value character varying(255) NOT NULL,
    client integer NOT NULL
);


ALTER TABLE public.dhcp4_clientoptions OWNER TO postgres;

--
-- Name: dhcp4_clientoptions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_clientoptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_clientoptions_id_seq OWNER TO postgres;

--
-- Name: dhcp4_clientoptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_clientoptions_id_seq OWNED BY public.dhcp4_clientoptions.id;


--
-- Name: dhcp4_knownclients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_knownclients (
    id integer NOT NULL,
    clientid character varying,
    value character varying NOT NULL,
    ipcount integer DEFAULT 1 NOT NULL,
    network integer NOT NULL,
    activated timestamp without time zone DEFAULT now()
);


ALTER TABLE public.dhcp4_knownclients OWNER TO postgres;

--
-- Name: dhcp4_knownclients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_knownclients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_knownclients_id_seq OWNER TO postgres;

--
-- Name: dhcp4_knownclients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_knownclients_id_seq OWNED BY public.dhcp4_knownclients.id;


--
-- Name: dhcp4_leases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_leases (
    id integer NOT NULL,
    ipaddress inet NOT NULL,
    macaddress macaddr NOT NULL,
    option82 character varying,
    subnet integer NOT NULL,
    leasetime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.dhcp4_leases OWNER TO postgres;

--
-- Name: dhcp4_leases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_leases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_leases_id_seq OWNER TO postgres;

--
-- Name: dhcp4_leases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_leases_id_seq OWNED BY public.dhcp4_leases.id;


--
-- Name: dhcp4_networks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_networks (
    id integer NOT NULL,
    network character varying NOT NULL,
    relayagent inet NOT NULL
);


ALTER TABLE public.dhcp4_networks OWNER TO postgres;

--
-- Name: dhcp4_networks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_networks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_networks_id_seq OWNER TO postgres;

--
-- Name: dhcp4_networks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_networks_id_seq OWNED BY public.dhcp4_networks.id;


--
-- Name: dhcp4_pooloptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_pooloptions (
    id integer NOT NULL,
    option smallint NOT NULL,
    value character varying(255) NOT NULL,
    subnet integer NOT NULL
);


ALTER TABLE public.dhcp4_pooloptions OWNER TO postgres;

--
-- Name: dhcp4_pooloptions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_pooloptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_pooloptions_id_seq OWNER TO postgres;

--
-- Name: dhcp4_pooloptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_pooloptions_id_seq OWNED BY public.dhcp4_pooloptions.id;


--
-- Name: dhcp4_pools; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_pools (
    id integer NOT NULL,
    poolstart inet NOT NULL,
    poolend inet NOT NULL,
    subnet integer NOT NULL
);


ALTER TABLE public.dhcp4_pools OWNER TO postgres;

--
-- Name: dhcp4_pools_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_pools_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_pools_id_seq OWNER TO postgres;

--
-- Name: dhcp4_pools_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_pools_id_seq OWNED BY public.dhcp4_pools.id;


--
-- Name: dhcp4_staticips; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_staticips (
    id integer NOT NULL,
    ipaddress inet NOT NULL,
    client integer NOT NULL
);


ALTER TABLE public.dhcp4_staticips OWNER TO postgres;

--
-- Name: dhcp4_staticips_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_staticips_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_staticips_id_seq OWNER TO postgres;

--
-- Name: dhcp4_staticips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_staticips_id_seq OWNED BY public.dhcp4_staticips.id;


--
-- Name: dhcp4_subnets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_subnets (
    id integer NOT NULL,
    subnet cidr NOT NULL,
    allowunknown boolean DEFAULT false NOT NULL,
    isstatic boolean DEFAULT false NOT NULL,
    network integer NOT NULL
);


ALTER TABLE public.dhcp4_subnets OWNER TO postgres;

--
-- Name: dhcp4_subnets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_subnets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_subnets_id_seq OWNER TO postgres;

--
-- Name: dhcp4_subnets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_subnets_id_seq OWNED BY public.dhcp4_subnets.id;


--
-- Name: dhcp4_vendoroptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dhcp4_vendoroptions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    suboption smallint NOT NULL,
    value character varying(255) NOT NULL,
    type integer NOT NULL
);


ALTER TABLE public.dhcp4_vendoroptions OWNER TO postgres;

--
-- Name: dhcp4_vendoroptions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dhcp4_vendoroptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dhcp4_vendoroptions_id_seq OWNER TO postgres;

--
-- Name: dhcp4_vendoroptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dhcp4_vendoroptions_id_seq OWNED BY public.dhcp4_vendoroptions.id;


--
-- Name: vendortypes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vendortypes (
    id integer NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.vendortypes OWNER TO postgres;

--
-- Name: vendortypes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vendortypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendortypes_id_seq OWNER TO postgres;

--
-- Name: vendortypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vendortypes_id_seq OWNED BY public.vendortypes.id;


--
-- Name: dhcp4_clientoptions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_clientoptions ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_clientoptions_id_seq'::regclass);


--
-- Name: dhcp4_knownclients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_knownclients ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_knownclients_id_seq'::regclass);


--
-- Name: dhcp4_leases id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_leases ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_leases_id_seq'::regclass);


--
-- Name: dhcp4_networks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_networks ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_networks_id_seq'::regclass);


--
-- Name: dhcp4_pooloptions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pooloptions ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_pooloptions_id_seq'::regclass);


--
-- Name: dhcp4_pools id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pools ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_pools_id_seq'::regclass);


--
-- Name: dhcp4_staticips id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_staticips ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_staticips_id_seq'::regclass);


--
-- Name: dhcp4_subnets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_subnets ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_subnets_id_seq'::regclass);


--
-- Name: dhcp4_vendoroptions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_vendoroptions ALTER COLUMN id SET DEFAULT nextval('public.dhcp4_vendoroptions_id_seq'::regclass);


--
-- Name: vendortypes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vendortypes ALTER COLUMN id SET DEFAULT nextval('public.vendortypes_id_seq'::regclass);


--
-- Name: dhcp4_clientoptions dhcp4_clientoptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_clientoptions
    ADD CONSTRAINT dhcp4_clientoptions_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_knownclients dhcp4_knownclients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_knownclients
    ADD CONSTRAINT dhcp4_knownclients_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_knownclients dhcp4_knownclients_value_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_knownclients
    ADD CONSTRAINT dhcp4_knownclients_value_key UNIQUE (value);


--
-- Name: dhcp4_leases dhcp4_leases_ipaddress_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_leases
    ADD CONSTRAINT dhcp4_leases_ipaddress_key UNIQUE (ipaddress);


--
-- Name: dhcp4_leases dhcp4_leases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_leases
    ADD CONSTRAINT dhcp4_leases_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_networks dhcp4_networks_network_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_networks
    ADD CONSTRAINT dhcp4_networks_network_key UNIQUE (network);


--
-- Name: dhcp4_networks dhcp4_networks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_networks
    ADD CONSTRAINT dhcp4_networks_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_networks dhcp4_networks_relayagent_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_networks
    ADD CONSTRAINT dhcp4_networks_relayagent_key UNIQUE (relayagent);


--
-- Name: dhcp4_pooloptions dhcp4_pooloptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pooloptions
    ADD CONSTRAINT dhcp4_pooloptions_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_pools dhcp4_pools_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pools
    ADD CONSTRAINT dhcp4_pools_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_pools dhcp4_pools_poolend_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pools
    ADD CONSTRAINT dhcp4_pools_poolend_key UNIQUE (poolend);


--
-- Name: dhcp4_pools dhcp4_pools_poolstart_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pools
    ADD CONSTRAINT dhcp4_pools_poolstart_key UNIQUE (poolstart);


--
-- Name: dhcp4_pools dhcp4_pools_subnet_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pools
    ADD CONSTRAINT dhcp4_pools_subnet_key UNIQUE (subnet);


--
-- Name: dhcp4_staticips dhcp4_staticips_client_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_staticips
    ADD CONSTRAINT dhcp4_staticips_client_key UNIQUE (client);


--
-- Name: dhcp4_staticips dhcp4_staticips_ipaddress_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_staticips
    ADD CONSTRAINT dhcp4_staticips_ipaddress_key UNIQUE (ipaddress);


--
-- Name: dhcp4_staticips dhcp4_staticips_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_staticips
    ADD CONSTRAINT dhcp4_staticips_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_subnets dhcp4_subnets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_subnets
    ADD CONSTRAINT dhcp4_subnets_pkey PRIMARY KEY (id);


--
-- Name: dhcp4_subnets dhcp4_subnets_subnet_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_subnets
    ADD CONSTRAINT dhcp4_subnets_subnet_key UNIQUE (subnet);


--
-- Name: dhcp4_vendoroptions dhcp4_vendoroptions_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_vendoroptions
    ADD CONSTRAINT dhcp4_vendoroptions_name_key UNIQUE (name);


--
-- Name: dhcp4_vendoroptions dhcp4_vendoroptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_vendoroptions
    ADD CONSTRAINT dhcp4_vendoroptions_pkey PRIMARY KEY (id);


--
-- Name: vendortypes vendortypes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vendortypes
    ADD CONSTRAINT vendortypes_pkey PRIMARY KEY (id);


--
-- Name: vendortypes vendortypes_type_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vendortypes
    ADD CONSTRAINT vendortypes_type_key UNIQUE (type);


--
-- Name: dhcp4_clientoptions dhcp4_clientoptions_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_clientoptions
    ADD CONSTRAINT dhcp4_clientoptions_client_fkey FOREIGN KEY (client) REFERENCES public.dhcp4_knownclients(id);


--
-- Name: dhcp4_knownclients dhcp4_knownclients_network_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_knownclients
    ADD CONSTRAINT dhcp4_knownclients_network_fkey FOREIGN KEY (network) REFERENCES public.dhcp4_networks(id);


--
-- Name: dhcp4_leases dhcp4_leases_subnet_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_leases
    ADD CONSTRAINT dhcp4_leases_subnet_fkey FOREIGN KEY (subnet) REFERENCES public.dhcp4_subnets(id);


--
-- Name: dhcp4_pooloptions dhcp4_pooloptions_subnet_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pooloptions
    ADD CONSTRAINT dhcp4_pooloptions_subnet_fkey FOREIGN KEY (subnet) REFERENCES public.dhcp4_subnets(id);


--
-- Name: dhcp4_pools dhcp4_pools_subnet_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_pools
    ADD CONSTRAINT dhcp4_pools_subnet_fkey FOREIGN KEY (subnet) REFERENCES public.dhcp4_subnets(id);


--
-- Name: dhcp4_staticips dhcp4_staticips_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_staticips
    ADD CONSTRAINT dhcp4_staticips_client_fkey FOREIGN KEY (client) REFERENCES public.dhcp4_knownclients(id);


--
-- Name: dhcp4_subnets dhcp4_subnets_network_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_subnets
    ADD CONSTRAINT dhcp4_subnets_network_fkey FOREIGN KEY (network) REFERENCES public.dhcp4_networks(id);


--
-- Name: dhcp4_vendoroptions dhcp4_vendoroptions_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dhcp4_vendoroptions
    ADD CONSTRAINT dhcp4_vendoroptions_type_fkey FOREIGN KEY (type) REFERENCES public.vendortypes(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.16
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: vendortypes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vendortypes (id, type) FROM stdin;
1	String
2	Uint8
3	Uint16
4	Uint32
5	IP-Address
6	Bool
7	Hex
\.


--
-- Name: vendortypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vendortypes_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--

